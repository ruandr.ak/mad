from collections import defaultdict

import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
from sklearn.tree import DecisionTreeRegressor


sns.set(style='darkgrid')


def score(clf, x, y):
    return roc_auc_score(y == 1, clf.predict_proba(x)[:, 1])


class Boosting:

    def __init__(
            self,
            base_model_params: dict = None,
            n_estimators: int = 10,
            learning_rate: float = 0.1,
            subsample: float = 0.3,
            early_stopping_rounds: int = None,
            plot: bool = False,
    ):
        self.base_model_class = DecisionTreeRegressor
        self.base_model_params: dict = {} if base_model_params is None else base_model_params

        self.n_estimators: int = n_estimators

        self.models: list = []
        self.gammas: list = []

        self.learning_rate: float = learning_rate
        self.subsample: float = subsample

        self.early_stopping_rounds: int = early_stopping_rounds
        if early_stopping_rounds is not None:
            self.validation_loss = np.full(self.early_stopping_rounds, np.inf)

        self.plot: bool = plot

        self.history = defaultdict(list)

        self.sigmoid = lambda x: 1 / (1 + np.exp(-x))
        self.loss_fn = lambda y, z: -np.log(self.sigmoid(y * z)).mean()
        self.loss_derivative = lambda y, z: -y * self.sigmoid(-y * z)
        self.loss_derivative2 = lambda y, z: y ** 2 * self.sigmoid(-y * z) * (1 - self.sigmoid(-y * z))

    def fit_new_base_model(self, x, y, predictions):
        n_objects = int(self.subsample * x.shape[0])
        indicies = np.random.randint(low=0, high = x.shape[0], size = n_objects)
        x, y, predictions = x[indicies], y[indicies], predictions[indicies]
        base_model = self.base_model_class(**self.base_model_params).fit(x, y)
#         base_model.set_params(**self.base_model_params)
#         base_model.fit(x, y)
        best_gamma = self.find_optimal_gamma(y, predictions, base_model.predict(x))
        self.gammas.append(self.learning_rate * best_gamma)
        self.models.append(base_model)

    def fit(self, x_train, y_train, x_valid, y_valid):
        """
        :param x_train: features array (train set)
        :param y_train: targets array (train set)
        :param x_valid: features array (validation set)
        :param y_valid: targets array (validation set)
        """
        train_predictions = np.zeros(y_train.shape[0])
        valid_predictions = np.zeros(y_valid.shape[0])
        
        s = y_train
        for i in range(self.n_estimators):
            self.fit_new_base_model(x_train, s, train_predictions)
            train_predictions += self.gammas[-1] * self.models[-1].predict(x_train)
            valid_predictions += self.gammas[-1] * self.models[-1].predict(x_valid)
            s = -self.loss_derivative(
                y_train,
                train_predictions
            )
            self.history["train"].append(self.loss_fn(y_train, train_predictions))
            self.history["valid"].append(self.loss_fn(y_valid, valid_predictions))
            if self.early_stopping_rounds is not None:
                if i < self.early_stopping_rounds:
                    continue
                if np.abs(self.history["train"][-1] - self.history["train"][-2]) < 1e-3 or \
                    np.abs(self.history["valid"][-1] - self.history["valid"][-2]) < 1e-3:
                    break
        if self.plot:
            plt.plot(list(range(self.n_estimators)), self.history["train"], label='train_loss')
            plt.plot(list(range(self.n_estimators)), self.history["valid"], label='valid_loss')
            plt.legend()
            plt.show()
    def predict_proba(self, x):    
        main_model_pred = np.sum(
            [gamma * model.predict(x) for gamma, model in zip(self.gammas, self.models)],
            axis=0
        )
        pred_proba = np.array([[y:=1-self.sigmoid(x), 1-y] for x in main_model_pred]) #оператор присваивания для того, чтобы 2 раза не вызывать sigmoid
        return pred_proba
    
    def predict(self, x):
        y=self.predict_proba(x)
        res = np.zeros(len(y))
        for i in range(len(y)):
            res[i] = 1 if y[i][0] < y[i][1] else 0
        return res            

    def find_optimal_gamma(self, y, old_predictions, new_predictions) -> float:
        gammas = np.linspace(start=0, stop=1, num=100)
        losses = [self.loss_fn(y, old_predictions + gamma * new_predictions) for gamma in gammas]
        return gammas[np.argmin(losses)]

    def score(self, x, y):
        return score(self, x, y)

    @property
    def feature_importances_(self):
        shape_ = self.models[0].feature_importances_.shape[0]
        mean_feature_importances_ = np.zeros(shape_)
        for model in self.models:
            mean_feature_importances_ += model.feature_importances_
        mean_feature_importances_ /= shape_
        mean_feature_importances_ = mean_feature_importances_ / np.sum(mean_feature_importances_)
        return mean_feature_importances_