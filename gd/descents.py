from dataclasses import dataclass
from enum import auto
from enum import Enum
from typing import Dict
from typing import Type

import numpy as np


@dataclass
class LearningRate:
    lambda_: float = 1e-3
    s0: float = 1
    p: float = 0.5

    iteration: int = 0

    def __call__(self):
        """
        Calculate learning rate according to lambda (s0/(s0 + t))^p formula
        """
        self.iteration += 1
        return self.lambda_ * (self.s0 / (self.s0 + self.iteration)) ** self.p


class LossFunction(Enum):
    MSE = lambda y_pred, y_true: np.mean(np.power(y_pred-y_true,2))
    MAE = lambda y_pred, y_true: np.mean(np.abs(y_pred-y_true))
    LogCosh = lambda y_pred, y_true: np.sum(np.log(np.cosh(y_pred - y_true)))
    Huber = lambda y_pred, y_true, delta_: 1/2 * np.mean(np.power(y_pred-y_true,2)) if (np.abs(y_true-y_pred) <= delta_).all() else delta_ * (np.mean(np.abs(y_pred-y_true)) - 1/2*delta_)


class BaseDescent:
    """
    A base class and templates for all functions
    """

    def __init__(self, dimension: int, lambda_: float = 1e-3, loss_function: LossFunction = LossFunction.MSE, delta_=1e-2):
        """
        :param dimension: feature space dimension
        :param lambda_: learning rate parameter
        :param loss_function: optimized loss function
        """
        self.w: np.ndarray = np.random.rand(dimension)
        self.lr: LearningRate = LearningRate(lambda_=lambda_)
        self.loss_function: LossFunction = loss_function
        self.dimension_ = dimension
        self.delta_ = delta_

    def step(self, x: np.ndarray, y: np.ndarray) -> np.ndarray:
        return self.update_weights(self.calc_gradient(x, y))

    def update_weights(self, gradient: np.ndarray) -> np.ndarray:
        """
        Template for update_weights function
        Update weights with respect to gradient
        :param gradient: gradient
        :return: weight difference (w_{k + 1} - w_k): np.ndarray
        """
        pass

    def calc_gradient(self, x: np.ndarray, y: np.ndarray) -> np.ndarray:
        """
        Template for calc_gradient function
        Calculate gradient of loss function with respect to weights
        :param x: features array
        :param y: targets array
        :return: gradient: np.ndarray
        """
        pass

    def calc_loss(self, x: np.ndarray, y: np.ndarray) -> float:
        """
        Calculate loss for x and y with our weights
        :param x: features array
        :param y: targets array
        :return: loss: float
        """
        if self.loss_function == LossFunction.Huber:
            return self.loss_function(x,y, self.delta_)
        else:
            return self.loss_function(x,y)

    def predict(self, x: np.ndarray) -> np.ndarray:
        """
        Calculate predictions for x
        :param x: features array
        :return: prediction: np.ndarray
        """
        return np.dot(x,self.w)


class VanillaGradientDescent(BaseDescent):
    """
    Full gradient descent class
    """

    def update_weights(self, gradient: np.ndarray) -> np.ndarray:
        """
        :return: weight difference (w_{k + 1} - w_k): np.ndarray
        """
        antigrad_with_lr = -self.lr.__call__() * gradient
        self.w = self.w + antigrad_with_lr
        return antigrad_with_lr

    def calc_gradient(self, x: np.ndarray, y: np.ndarray) -> np.ndarray:
        if self.loss_function == LossFunction.MSE:
            return 2 * np.dot(x.T, np.dot(x, self.w) - y) / x.shape[0]
        elif self.loss_function == LossFunction.LogCosh:
            return np.dot(x.T, np.tanh(np.dot(x, self.w) - y))
        elif self.loss_function == LossFunction.MAE:
            return np.dot(x.T,-np.sign(y-np.dot(x, self.w))) / x.shape[0]
        else:
            return 1/2 * (2 * np.dot(x.T, np.dot(x, self.w) - y) / x.shape[0]) if (np.abs(y-np.dot(x, self.w)) <= self.delta_).all() else self.delta_ * (np.dot(x.T,-np.sign(y-np.dot(x, self.w))) / x.shape[0])

class StochasticDescent(VanillaGradientDescent):
    """
    Stochastic gradient descent class
    """

    def __init__(self, dimension: int, lambda_: float = 1e-3, batch_size: int = 50,
                 loss_function: LossFunction = LossFunction.MSE):
        """
        :param batch_size: batch size (int)
        """
        super().__init__(dimension, lambda_, loss_function)
        self.batch_size = batch_size

    def calc_gradient(self, x: np.ndarray, y: np.ndarray) -> np.ndarray:
        sample = np.random.randint(0,high=x.shape[0]-1, size=self.batch_size)
        if self.loss_function == LossFunction.MSE:
            return 2 * np.dot(x[sample].T, np.dot(x[sample], self.w) - y[sample]) / self.batch_size
        elif self.loss_function == LossFunction.LogCosh:
            return np.dot(x[sample].T, np.tanh(np.dot(x[sample], self.w) - y[sample]))
        elif self.loss_function == LossFunction.MAE:
            return np.dot(x[sample].T,-np.sign(y[sample]-np.dot(x[sample], self.w)))
        else:
            return 1/2 * (2 * np.dot(x[sample].T, np.dot(x[sample], self.w) - y[sample]) / self.batch_size) if (np.abs(y[sample]-np.dot(x[sample], self.w)) <= self.delta_).all() else self.delta_ * (np.dot(x[sample].T,-np.sign(y[sample]-np.dot(x[sample], self.w))))


class MomentumDescent(VanillaGradientDescent):
    """
    Momentum gradient descent class
    """

    def __init__(self, dimension: int, lambda_: float = 1e-3, loss_function: LossFunction = LossFunction.MSE):
        super().__init__(dimension, lambda_, loss_function)
        self.alpha: float = 0.9
        self.h: np.ndarray = np.zeros(dimension)

    def update_weights(self, gradient: np.ndarray) -> np.ndarray:
        """
        :return: weight difference (w_{k + 1} - w_k): np.ndarray
        h_0 = 0
        h_{k + 1} = \alpha h_{k} + \eta_k \nabla_w Q(w_{k})
        w_{k + 1} = w_{k} - h_{k + 1}
        """
        grad_with_lr = self.lr.__call__() * gradient #calc grad
        self.h = self.alpha * self.h + grad_with_lr # calc h_{k+1}
        self.w = self.w - self.h #calc w_{k+1}
        return -self.h


class Adam(VanillaGradientDescent):
    """
    Adaptive Moment Estimation gradient descent class
    """

    def __init__(self, dimension: int, lambda_: float = 1e-3, loss_function: LossFunction = LossFunction.MSE):
        super().__init__(dimension, lambda_, loss_function)
        self.eps: float = 1e-8

        self.m: np.ndarray = np.zeros(dimension)
        self.v: np.ndarray = np.zeros(dimension)

        self.beta_1: float = 0.9
        self.beta_2: float = 0.999

        self.iteration: int = 0

    def update_weights(self, gradient: np.ndarray) -> np.ndarray:
        """
        :return: weight difference (w_{k + 1} - w_k): np.ndarray
        """
        self.m = self.beta_1 * self.m + (1 - self.beta_1) * gradient
        self.v = self.beta_2 * self.v + (1 - self.beta_2) * np.power(gradient,2)
        self.iteration += 1
        m = self.m / (1 - self.beta_1 ** self.iteration) #m_{k+1}
        v = self.v / (1 - self.beta_2 ** self.iteration) #v_{k+1}
        tmp = self.lr.__call__() / (np.sqrt(v) + self.eps) * m #for each component from vector + eps
        self.w = self.w - tmp
        return -tmp
    
class Nadam(VanillaGradientDescent):
    def __init__(self, dimension: int, lambda_: float = 1e-3, loss_function: LossFunction = LossFunction.MSE):
        super().__init__(dimension, lambda_, loss_function)
        self.eps: float = 1e-8

        self.m: np.ndarray = np.zeros(dimension)
        self.v: np.ndarray = np.zeros(dimension)

        self.beta_1: float = 0.9
        self.beta_2: float = 0.999

        self.iteration: int = 0
            
    def update_weights(self, gradient: np.ndarray) -> np.ndarray:
        """
        :return: weight difference (w_{k + 1} - w_k): np.ndarray
        """
        self.m = self.beta_1 * self.m + (1 - self.beta_1) * gradient
        self.v = self.beta_2 * self.v + (1 - self.beta_2) * np.power(gradient, 2)
        self.iteration += 1
        m_hat = self.m / (1 - np.power(self.beta_1, self.iteration)) + (1 - self.beta_1) * gradient / (1 - np.power(self.beta_1, self.iteration))
        v_hat = self.v / (1 - np.power(self.beta_2, self.iteration))
        tmp = self.lr.__call__() * m_hat / (np.sqrt(v_hat) + self.eps)
        self.w = self.w - tmp
        return -tmp


class BaseDescentReg(BaseDescent):
    """
    A base class with regularization
    """

    def __init__(self, *args, mu: float = 0, **kwargs):
        """
        :param mu: regularization coefficient (float)
        """
        super().__init__(*args, **kwargs)

        self.mu = mu

    def calc_gradient(self, x: np.ndarray, y: np.ndarray) -> np.ndarray:
        """
        Calculate gradient of loss function and L2 regularization with respect to weights
        """
#         l2_gradient: np.ndarray = np.zeros_like(x.shape[1])  # TODO: replace with L2 gradient calculation
#         l2_gradient = 2 * self.w / x.shape[0]
        l2_gradient = self.w
        return super().calc_gradient(x, y) + l2_gradient * self.mu


class VanillaGradientDescentReg(BaseDescentReg, VanillaGradientDescent):
    """
    Full gradient descent with regularization class
    """


class StochasticDescentReg(BaseDescentReg, StochasticDescent):
    """
    Stochastic gradient descent with regularization class
    """


class MomentumDescentReg(BaseDescentReg, MomentumDescent):
    """
    Momentum gradient descent with regularization class
    """


class AdamReg(BaseDescentReg, Adam):
    """
    Adaptive gradient algorithm with regularization class
    """


def get_descent(descent_config: dict) -> BaseDescent:
    descent_name = descent_config.get('descent_name', 'full')
    regularized = descent_config.get('regularized', False)

    descent_mapping: Dict[str, Type[BaseDescent]] = {
        'full': VanillaGradientDescent if not regularized else VanillaGradientDescentReg,
        'stochastic': StochasticDescent if not regularized else StochasticDescentReg,
        'momentum': MomentumDescent if not regularized else MomentumDescentReg,
        'adam': Adam if not regularized else AdamReg,
        'nadam': Nadam
    }

    if descent_name not in descent_mapping:
        raise ValueError(f'Incorrect descent name, use one of these: {descent_mapping.keys()}')

    descent_class = descent_mapping[descent_name]

    return descent_class(**descent_config.get('kwargs', {}))
